import React from 'react'
import HomeScreen from '../screens/Home'
import {
  createBottomTabNavigator,
  createStackNavigator
} from 'react-navigation'

import BottomBar from '../components/Tab/Bottom'

const MainTab = createStackNavigator({
  Home: {
    screen: HomeScreen,
    navigationOptions: {
      headerTransparent: true,
      headerTitleStyle: { color: '#FFF', fontSize: 24 }
    }
  }
})

const TabNavigator = createBottomTabNavigator(
  {
    Home: MainTab,
    Sales: {
      screen: HomeScreen
    }
  },
  {
    tabBarComponent: props => <BottomBar {...props} />,
    tabBarOptions: {
      style: {
        backgroundColor: 'transparent',
        borderTopWidth: 0,
        position: 'absolute',
        left: 0,
        right: 0,
        bottom: 0,
        height: 50
      }
    },
    initialRouteName: 'Home'
  }
)

export default TabNavigator
