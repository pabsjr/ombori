import { createAppContainer, createSwitchNavigator } from 'react-navigation'

import TabNavigator from './TabNavigator'

const switchNavigator = createSwitchNavigator(
  {
    Tab: {
      screen: TabNavigator
    }
  },
  {
    initialRouteName: 'Tab'
  }
)

export const getRootNavigator = () => createAppContainer(switchNavigator)
