import React from 'react'
import { TouchableOpacity, View } from 'react-native'
import { Icon } from 'react-native-elements'
import TranslucentView from '../TransluscentView'

class BottomBar extends React.Component {
  render () {
    const {
      navigation: {
        state: { index, routes }
      },
      jumpTo,
      style
    } = this.props

    const colors = {
      active: 'blue',
      inactive: '#6A6A6A'
    }

    const icons = {
      Home: 'home',
      Sales: 'info'
    }

    return (
      <TranslucentView
        style={{
          ...style,
          width: '100%',
          height: 70,
          backgroundColor: 'transparent'
        }}
      >
        <View
          style={{
            display: 'flex',
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'center',
            height: '100%'
          }}
        >
          {routes.map((route, idx) => (
            <TouchableOpacity
              key={`list-item-${idx}`}
              style={{
                flexGrow: 1,
                alignItems: 'center',
                justifyContent: 'center'
              }}
              onPress={() => jumpTo(route.key)}
            >
              <View>
                <Icon
                  name={icons[route.key]}
                  size={20}
                  type='feather'
                  color={index === idx ? colors.active : colors.inactive}
                />
              </View>
            </TouchableOpacity>
          ))}
        </View>
      </TranslucentView>
    )
  }
}

export default BottomBar
