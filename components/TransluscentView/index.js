import React from 'react'
import { BlurView } from '@react-native-community/blur'

class TranslucentView extends React.Component {
  render () {
    const { children, ...rest } = this.props
    const blurViewProps = {
      blurType: 'light',
      blurAmount: 60,
      ...rest
    }

    return <BlurView {...blurViewProps}>{children}</BlurView>
  }
}

export default TranslucentView
