import React from 'react'
import PulseLoader from './Loader'

export default class ActivityIndicator extends React.Component {
  render () {
    return (
      <PulseLoader
        borderColor='#d0e6a3'
        backgroundColor='#d0e6a3'
        interval={2000}
        size={50}
        pulseMaxSize={150}
        dotStyle={{ backgroundColor: '#7fb900' }}
      />
    )
  }
}
