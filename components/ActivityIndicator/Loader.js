import React from 'react'
import { View, Animated } from 'react-native'
import Pulse from './Pulse'

export default class PulseLoader extends React.Component {
  constructor (props) {
    super(props)

    this.state = {
      circles: []
    }

    this.counter = 1
    this.setInterval = null
    this.anim = new Animated.Value(1)
  }

  componentDidMount () {
    this.setCircleInterval()

    setInterval(() => {
      this.addCircle()
    }, this.props.interval * 0.75)
  }

  setCircleInterval () {
    this.setInterval = setInterval(() => {
      this.addCircle()
    }, this.props.interval)
  }

  addCircle () {
    this.setState({ circles: [...this.state.circles, this.counter] })
    this.counter++
  }

  render () {
    const { dotStyle } = this.props
    const dotProps = {
      style: {
        width: 30,
        height: 30,
        backgroundColor: '#000',
        borderRadius: 100,
        ...dotStyle
      }
    }

    return (
      <View
        style={{
          flex: 1,
          backgroundColor: 'transparent',
          justifyContent: 'center',
          alignItems: 'center'
        }}
      >
        {this.state.circles.map(circle => (
          <Pulse key={circle} {...this.props} />
        ))}

        <View {...dotProps} />
      </View>
    )
  }
}

PulseLoader.defaultProps = {
  interval: 2000,
  size: 100,
  pulseMaxSize: 250,
  borderColor: '#D8335B'
}
