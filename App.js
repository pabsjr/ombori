/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React from "react"
import { getRootNavigator } from "./navigation"

export default class RootApp extends React.Component {
  render() {
    const RootNavigator = getRootNavigator()

    return <RootNavigator />
  }
}
