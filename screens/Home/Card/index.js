import React from 'react'
import { View, Text, ActivityIndicator, StyleSheet } from 'react-native'
import { Image } from 'react-native-elements'

import TranslucentView from '../../../components/TransluscentView'
class Card extends React.Component {
  render () {
    const { item } = this.props

    return (
      <View>
        <View style={styles.cardWrapper}>
          <TranslucentView
            blurType='light'
            blurAmount={20}
            style={styles.cardView}
          >
            <Text style={styles.cardTitle}>{item.first_name}</Text>
            <Text style={styles.cardSubtitle}>{item.email}</Text>
          </TranslucentView>
          <Image
            style={styles.image}
            source={{ uri: item.avatar }}
            resizeMode='cover'
            PlaceholderContent={<ActivityIndicator />}
          />
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  image: {
    width: '100%',
    height: 500
  },
  cardWrapper: {
    position: 'relative',
    borderRadius: 30,
    overflow: 'hidden',
    marginBottom: 24
  },
  cardView: {
    padding: 24,
    paddingTop: 30,
    paddingBottom: 30,
    position: 'absolute',
    bottom: 0,
    left: 0,
    zIndex: 9,
    width: '100%'
  },
  cardTitle: {
    color: '#fff',
    fontSize: 24,
    fontWeight: 'bold'
  },
  cardSubtitle: {
    color: '#fff',
    fontSize: 12
  }
})

export default Card
