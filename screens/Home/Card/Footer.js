import React from 'react'
import { View, Text, ActivityIndicator, StyleSheet } from 'react-native'

class CardFooter extends React.Component {
  render () {
    const { isLastPage, loading } = this.props

    return (
      <View style={styles.cardFooter}>
        {isLastPage && (
          <Text style={styles.emptyText}>There are no more users</Text>
        )}
        {loading && <ActivityIndicator />}
      </View>
    )
  }
}

const styles = StyleSheet.create({
  cardFooter: {
    height: 100,
    marginBottom: 120,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center'
  },
  emptyText: {
    color: '#FFF',
    textAlign: 'center',
    fontSize: 20
  }
})

export default CardFooter
