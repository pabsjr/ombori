import { requests } from '../../utils/httpClient'
import qs from 'qs-stringify'

export const home = {
  get: options => {
    const config = {
      per_page: 3,
      ...options
    }

    return requests.get(`/users?${qs(config)}`)
  }
}
