import React from 'react'
import { Text, View, FlatList, StyleSheet } from 'react-native'

import createPage from '../../utils/createPage'

import { home } from './endpoint'
import Card from './Card'
import CardFooter from './Card/Footer'
class Home extends React.Component {
  state = {
    isFetching: false,
    data: [],
    currentPage: 1,
    totalPages: 0,
    hasMore: true
  }

  componentDidMount () {
    this.fetchItems({ page: 1 })
  }

  async fetchItems (params = {}) {
    this.setState({
      isFetching: true
    })

    try {
      const response = await home.get(params)

      if (response.ok) {
        const { data } = response
        const hasMore = data.page <= data.total_pages

        let pageOptions = {
          isFetching: false
        }

        if (hasMore) {
          pageOptions = {
            ...pageOptions,
            data: [...this.state.data, ...data.data],
            currentPage: data.page,
            totalPages: data.total_pages,
            hasMore
          }
        }

        this.setState(pageOptions)
      }
    } catch (err) {
      console.log('err', err)
    }
  }

  loadMoreUsers () {
    const { hasMore, currentPage, isFetching } = this.state

    if (hasMore && !isFetching) {
      this.fetchItems({
        page: currentPage + 1
      })
    }
  }

  isCloseToBottom (nativeEvent) {
    const { layoutMeasurement, contentOffset, contentSize } = nativeEvent

    return layoutMeasurement.height + contentOffset.y >= contentSize.height - 50
  }

  render () {
    const { data, isFetching, currentPage, totalPages } = this.state
    const isLastPage = currentPage === totalPages

    return (
      <View style={styles.container}>
        <FlatList
          numColumns={1}
          data={data}
          keyExtractor={value => value.id}
          onEndReached={() => this.loadMoreUsers()}
          renderItem={value => <Card {...value} />}
          ListHeaderComponent={() => (
            <View style={styles.cardHeader}>
              <Text style={styles.cardHeaderTitle}>My Contacts</Text>
            </View>
          )}
          ListFooterComponent={() => (
            <CardFooter isLastPage={isLastPage} loading={isFetching} />
          )}
          onEndReachedThreshold={0.1}
        />
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#000',
    height: '100%'
  },
  cardHeader: {
    margin: 24,
    marginTop: 80
  },
  cardHeaderTitle: {
    fontSize: 30,
    color: '#FFF',
    fontWeight: 'bold'
  }
})

export default createPage({
  navigationOptions: {
    header: null
  }
})(Home)
