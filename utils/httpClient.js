import { create } from 'apisauce'

import config from '../ombori.config.json'

export const api = create({
  baseURL: config.api.baseURL + '/api/',
  headers: {
    Accept: 'application/json'
  }
})

export const requests = {
  get: async url => {
    const result = await api.get(url)

    return result
  }
}
