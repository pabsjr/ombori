import React, { Component } from 'react'
import { View, StatusBar } from 'react-native'

import ActivityIndicator from '../components/ActivityIndicator'

export default options => Component => {
  options = {
    withHeader: true,
    ...options
  }

  return createPage(options, Component)
}

const createPage = (options, InnerPage) => {
  class Page extends Component {
    constructor (props) {
      super(props)

      this.state = {
        loading: true
      }
    }

    componentDidMount () {
      setTimeout(() => {
        this.setState({ loading: false })
      }, 3000)
    }

    render () {
      const { loading } = this.state

      if (loading) {
        return (
          <View style={{ height: '100%' }}>
            <ActivityIndicator />
          </View>
        )
      }

      return (
        <View>
          <StatusBar barStyle='light-content' translucent />
          <InnerPage {...this.props} />
        </View>
      )
    }
  }

  if (options.navigationOptions) {
    Page.navigationOptions = options.navigationOptions
  }

  return Page
}
